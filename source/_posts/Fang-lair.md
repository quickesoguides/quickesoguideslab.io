---
title: Fang Lair
date: 2020-05-24 10:45:24
tags: dungeon, dungeon guide
---

Fang Lair is a DLC dungeon in ESO.

# Bosses

## First - Lizabet Charnis
![Lizabet](/img/f1.jpg)

No mechanics for this boss. It will send several waves of trash mobs, which you need to kill. 

Be sure to pick the chest with rewards near the stairs.

**Tank:** Try to pick Bone Colossus mobs. They are fat and can deal good damage.

<!-- more -->

## Second - Cadaverous Bear

![Bear](/img/fl2.jpg)

One main boss (Cadaverous Bear) and 2 mini-bosses (Guar and Tiger). Small wolves will spawn and explode near players. You need to interrupt Tiger when he jumps on someone and starts eating them. When Tiger is about to jump, it inflicts Fear upon you.

**Tank:** Tiger and wolves can't be taunted. Focus on holding Bear and Guar.

**DD:** Interrupt Tiger.

## Third - Caluurion

![Caluurion](/img/fl3_1.jpg)

Lich that stands in the middle and sends AOE attacks. At one point it will spawn a totem (relic), which gives you and other players AOE fields. Don't stack so that you don't overlap them. Kill totem ASAP to remove those AOE fields.

**DD:** Focus on relic when it appears.

## Fourth - Ulfnor

![ulfnorr](/img/fl4.jpg)

Just a regular boss that will spawn Ghost from time to time. Ghost will catch a player in chains and slowly drag that player to himself. When chained player gets near ghost, it will instantly kill the player, so Ghost must be killed ASAP.

**Tank:** Block Ulfnor's heavy attacks! They are deadly.

**DD:** Focus on Ghost when it catches a player in chains.

## Final - Thurvokun (Dragon)


![Thurvokun](/img/fl5.jpg)

This guide will discuss normal mode, when you fight only skeletal dragon, Thurvokun. In hard mode, it will be joined by necromancer, Orryn the Black.

Necromancer (Orryn) will activate crystals at 85%, 75%, 65% and 55% HP of the dragon. That crystal must be killed ASAP. If left unchecked it will spawn a lot of adds.

In the next phase Necromancer will summon walls of ghosts. They are deadly - you must hide behind the golden barrier, that will be put up by a friendly golden ghost.