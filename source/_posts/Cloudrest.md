---
title: Cloudrest - Normal
date: 2020-06-29 03:03:45
tags: trial, trial guide
---

Cloudrest is a trial in Summerset. In its normal mode it's very easy so it's popular with pugs. Group setup is 1 Tank, 2 Healers and 9 DDs for +0. For +2 and higher, you might go with 2 Tanks, main one for Z’Maja and off-tank for mini boss. [Raid notifier](https://www.esoui.com/downloads/info1355-RaidNotifierUpdated.html) addon is highly recommended.

# Glossary

*Spears* - shining spear-like items that are needed to close portal in Shadow world.

![Spear](/img/nCR/spear.jpg)

*+0, +1, ...* - how many mini-bosses you fight with final boss (Z’Maja). Most popular options - none (+0) and all of them (+3). Loot is better when fighting more bosses.

*Downstairs* - in the Shadow world, i.e. in portal.

# Bosses

All bosses except the last one (Z’Maja) are almost identical. It will be a melee fighter and a gryphon.
<!-- more -->
## Shade of Galenwe

![Galenwe](/img/nCR/galenwe.jpg)

Deals ice damage.

#### Hoarfrost

At some point random player will get Hoarfrost debuff, which deals damage to the player and slows him down. That player needs to shed it (press X).

**Tank:** taunt both Galenwe and his gryphon. You may have to range taunt gryphon as it sometimes flies up.

## Shade of Relequen

![Relequen](/img/nCR/Relequen.jpg)

Deals shock damage.

#### Voltaic overload

Random player will be targeted with voltaic overload. **You will have 3 seconds to decide which weapon bar you want to be on.**  Then you need to swap weapon and don't swap it back until 10s have passed, or you will have a large AOE around you. Raid notifier makes it trivial, otherwise you'll really need to watch out for a blueish glow around you (which means you got the debuff).

**Tank:** taunt both Relequen and his gryphon. You may have to range taunt gryphon as it sometimes flies up.

## Shade of Siroria

![Siroria](/img/nCR/siroria.jpg)

Deals fire damage.

#### Roaring flare

Random player will get a flare debuff, then he should run **into** the group of people, so that damage is spread to everybody. Otherwise the said player will get one-shot.

**Tank:** taunt both Siroria and her gryphon. You may have to range taunt gryphon as it sometimes flies up.

## Z’Maja

Final boss. *Portal group* is chosen (3 DDs) who will enter portal when it appears. The rest stay in real world and fight boss and adds.

**DD (real world):** kill priority is as follows: adds (tentactles, monsters, spheres) > main boss. When you see Spears, you run to them and activate syngergy to send them into Shadow world.

### Portal group/

3 DDs go into portal (shadow world). They need to destroy crystals (shards), then take Malevolent Core orbs that spawn when crystal is destroyed and bring them to Spears to activate. 
Crystals look like this:
![Shadow world crystal](/img/nCR/crystal.jpg)

There are more than 3 crystals, so not every crystal contains an orb.

Shadow Z’Maja will hit everybody with AOE sometimes, so if you have low HP or no self-heal, use *Wind of the Welkynar* synergy to jump above, avoiding the AOE.
![Wind of the Welkynar](/img/nCR/wind.jpg)

When you've activated 3 Spears, portal will close and you'll get back to real world.

# Modes

## +0

Easiest mode - you kill all mini-bosses before going to Z’Maja. Follow the guide above.

## +1, +2, +3

At 75%, 50% and 25% HP Z’Maja will spawn an additional mini-boss. While possible with one Tank, usually offtank (OT) role is introduced.

**DD (real world):** your kill priority changes: adds > mini-boss > main boss. Otherwise it's the same. Stop all dps on Z’Maja when mini-boss spawns.

**Tank (offtank):** you tank the mini-boss away from Z’Maja.