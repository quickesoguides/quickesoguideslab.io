---
title: Crypt of Hearts I
date: 2020-05-29 17:45:24
tags: dungeon, dungeon guide
---

Crypt of Hearts 1 is a non-DLC dungeon in ESO. Along with CoH 2 it's famous for Ebon Armory set that drops there. It's one of the easier dungeons (both in Normal and Veteran).

# Bosses

## First - The Mage Master
![The Mage Master](/img/coh1/coh1.jpg)

No mechanics for this boss.

**Tank:** block heavy attacks and keep adds with boss (to help AOE dps).
<!-- more -->

## Second - Archmaster Siniel
![Archmaster Siniel](/img/coh1/coh2.jpg)

No mechanics for this boss.

## Third - Death’s Leviathan
![Death’s Leviathan prepares to charge](/img/coh1/coh3.jpg)

Non-tank players should avoid AOE. Boss will charge sometimes, get out of his path.

**Tank:** you can tank Leviathan near the wall so he won't have much space to charge.

## Miniboss - Uulkar Bonehand
![Dogas the Berserker](/img/coh1/cohU.jpg)

No mechanics for this boss.

## Fourth - Dogas the Berserker
![Dogas the Berserker](/img/coh1/cohD.jpg)

No mechanics for this boss.

**Tank:** block heavy attacks.

## Final - Ilambris-Zaven & Ilambris-Athor
![Dogas the Berserker](/img/coh1/coh5.jpg)

Kill mage (Zaven) first, as he has a nasty AOE that knocks down everybody.

**Tank:** focus on holding fighter (Athor), block his heavy attacks. Mage (Zaven) will stand in the middle. You can either come off the platform and avoid mage's AOE or hold them together for better dps.

**DD:** focus mage, then fighter.